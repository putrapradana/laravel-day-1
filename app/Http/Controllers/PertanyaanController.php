<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index(){
        // $daftarpertanyaan = DB::table('pertanyaan')->get();
        $daftarpertanyaan = Pertanyaan::all();

        return view('pertanyaan.index', compact('daftarpertanyaan'));
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')->insert(
        //     [
        //         "judul" => $request['judul'],
        //         "isi" => $request['isi']
        //     ]
        //     );

        $query = Pertanyaan::create(
            [
                "judul" => $request['judul'],
                "isi" => $request['isi']
            ]
            );
        return redirect('pertanyaan')->with('success', 'Pertanyaan Berhasil Dibuat!');
    }

    public function show($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->first();
        $query = Pertanyaan::find($id);
        // $query = Pertanyaan::where('id', $id)->first();

        return view('pertanyaan.show', compact('query'));
    }

    public function edit($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->first();
        $query = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('query'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update(
        //     [
        //         "judul" => $request['judul'],
        //         "isi" => $request['isi']
        //     ]
        //     );
        $query = Pertanyaan::where('id', $id)->update(
            [
                "judul" => $request['judul'],
                "isi" => $request['isi']
            ]
            );

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diperbarui!');
    }

    public function destroy($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
