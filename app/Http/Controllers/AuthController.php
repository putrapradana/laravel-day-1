<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function form(Request $request){
        // dd($request->all());
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        return view('dashboard', compact('firstName','lastName'));
    }

}
