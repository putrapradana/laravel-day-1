@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Pertanyaan</small></h3>
        </div>
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" name="judul" class="form-control" id="judul" placeholder="Enter Judul" value="{{old('judul','')}}">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <textarea name="isi" class="form-control" id="isi" placeholder="Enter Isi" value="{{old('isi','')}}" rows="10" cols="200"></textarea>
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
    
@endsection