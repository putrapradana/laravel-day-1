@extends('adminlte.master')

@section('content')
    <!-- /.card-header -->
    <div class="card-header">
        <h3 class="card-title">Tabel Pertanyaan</h3>
    </div>
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>   
        @endif
        {{-- <a class="btn btn-primary mb-3" href="/pertanyaan/create">Create New</a> --}}
        <a class="btn btn-primary mb-3" href="{{route('pertanyaan.create')}}">Create New</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($daftarpertanyaan as $key => $pertanyaan)
                    <tr>
                        <td>{{$pertanyaan->id}}</td>
                        <td>{{$pertanyaan->judul}}</td>
                        <td>{{$pertanyaan->isi}}</td>
                        <td style="display: flex">
                            {{-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">show</a> --}}
                            <a href="{{route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-info btn-sm">show</a>
                            <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr> 
                @empty
                    <tr>
                        <td colspan="4" align="center"> No Data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->    
@endsection