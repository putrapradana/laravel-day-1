@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Show Pertanyaan {{$query->id}}</h3>
        </div>
        <form role="form" action="/pertanyaan/{{$query->id}}" method="POST">
            <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" name="judul" class="form-control" id="judul" placeholder="Enter Judul" value="{{old('judul', $query->judul)}}" readonly>
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <textarea name="isi" class="form-control" id="isi" placeholder="Enter Isi" rows="10" cols="200" readonly>{{old('isi', $query->isi)}}</textarea>
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            </div>
        </form>
    </div>
    
@endsection