@extends('adminlte.master')

    @section('content')
    <h1 style="text-align:center;font-family:verdana;">Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/form" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" id="firstName" name="firstName"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" id="lastName" name="lastName"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label>Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label>Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label>Other</label><br><br>
        <label>Nasionality:</label><br><br>
        <select id="nasional" name="nasional">
          <option value="ina">Indonesian</option>
          <option value="eng">England</option>
          <option value="bra">Brazilian</option>
          <option value="other">Other</option>
        </select> <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" id="ina" name="nasional" value="ina">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" id="eng" name="nasional" value="eng">
        <label>English</label><br>
        <input type="checkbox" id="other" name="nasional" value="other">
        <label for="other">Other</label><br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio"  style="width:300px; height:200px;">
        </textarea> 
        <br>
        <input type="submit" value="Submit">
    </form>
    @endsection
